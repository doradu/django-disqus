from os.path import normpath, join, dirname, abspath

SECRET_KEY = 'fake-key'

DATABASE_ENGINE = 'sqlite3'
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sessions',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'disqus',
)
TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [
        normpath(join(dirname(dirname(abspath(__file__))), 'disqus', 'templates')),
    ],
    'OPTIONS': {
        'debug': False,
        'context_processors': (
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
            'django.template.context_processors.request'
        )
    },
}]

MIDDLEWARE = [
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware'
]

#!/usr/bin/env python
import os
import sys
from os.path import dirname, abspath

import django
from django.conf import settings
from django.test.utils import get_runner


def runtests(*test_args):
    if not test_args:
        test_args = ['disqus']
    test_runner_class = get_runner(settings)
    test_runner = test_runner_class()
    failures = test_runner.run_tests(test_args)
    sys.exit(failures)


if __name__ == '__main__':
    os.environ['DJANGO_SETTINGS_MODULE'] = 'disqus.test_settings'
    django.setup()
    runtests(*sys.argv[1:])
